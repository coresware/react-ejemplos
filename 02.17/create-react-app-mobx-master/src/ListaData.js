import { extendObservable } from 'mobx';

class ListaData {
  constructor() {
    extendObservable(this, {
      tareas: ['Aprender React', 'Hacer aplicación React']
    });
  }

  agregarTarea(tarea) {
  	console.log(tarea);
  	this.tareas.push(tarea);
  }
}

var VarListaData = new ListaData;
export default VarListaData;


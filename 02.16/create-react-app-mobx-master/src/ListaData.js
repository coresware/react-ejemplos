import { extendObservable } from 'mobx';

class ListaData {
  constructor() {
    extendObservable(this, {
      tareas: ['Aprender React', 'Hacer aplicación React']
    });
  }
}

var VarListaData = new ListaData;
export default VarListaData;

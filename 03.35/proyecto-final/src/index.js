import React from 'react';
import ReactDOM from 'react-dom';
import Lienzo from './Lienzo';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css';
import {BrowserRouter, Route, Link} from 'react-router-dom';

const RouterDeLienzo = (
		<BrowserRouter>
			<Route path="/" component={Lienzo}/>
		</BrowserRouter>
	);

ReactDOM.render(
  RouterDeLienzo, 
  document.getElementById('root')
);

import { extendObservable, computed } from 'mobx';

class TiendaController {
  constructor() {
    extendObservable(this, {
      {
        platillos: [
          {nombre:"primerPlatillo", 
            descripcion: "platillo muy rico", 
            precio:100, 
            cantidad:0},
          {nombre:"segundoPlatillo", 
            descripcion: "platillo muy rico", 
            precio:100, 
            cantidad:0},
          {nombre:"tercerPlatillo", 
            descripcion: "platillo muy rico", 
            precio:100, 
            cantidad:0}
        ]
      }
    });
  }
}

var VarTiendaController = new TiendaController;
export default VarTiendaController;
